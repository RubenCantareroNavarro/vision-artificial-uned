﻿//------------------------------------------------------------------------------
// <copyright file="App.xaml.cs" company="Rubén Cantarero Navarro">
//     Copyright (c) Rubén Cantarero Navarro.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace KinBehR.ImageCaptureModule
{
    using System;
    using System.Windows;

    /// <summary>
    /// Interaction logic for App
    /// </summary>
    public partial class App : Application
    {
    }
}
