﻿//------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="Rubén Canraero Navarro">
//     Copyright (c) Rubén Canraero Navarro.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

namespace KinBehR.ImageCaptureModule
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using Microsoft.Kinect;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Shapes;
    using System.Windows.Controls;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Imaging;
    using SharpAvi;
    using SharpAvi.Output;
    using SharpAvi.Codecs;
    using System.Xml;
    using System.Xml.Serialization;
    using System.Windows.Forms;

    /// <summary>
    /// Interaction logic for MainWindow
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private TaskCompletionSource<bool> tcs = null;

        /// <summary>
        /// Quality of the videos.
        /// </summary>
        private int quality = 100;

        /// <summary>
        /// Frame rate of the videos.
        /// </summary>
        private double frame_rate = 20;

        /// <summary>
        /// List of tasks for save color frames
        /// </summary>
        private List<Task> colorTasks = null;

        /// <summary>
        /// List of tasks for save depth frames
        /// </summary>
        private List<Task> depthTasks = null;

        /// <summary>
        /// List of tasks for save infrared frames
        /// </summary>
        private List<Task> infraredTasks = null;

        /// <summary>
        /// List of tasks for save shadow frames
        /// </summary>
        private List<Task> shadowTasks = null;

        /// <summary>
        /// List of tasks for save xml files
        /// </summary>
        private List<Task> xmlTasks = null;

        /// <summary>
        /// AviWriter of color data
        /// </summary>
        private AviWriter colorWriter = null;
         
        /// <summary>
        /// IAviVideoStream of color data
        /// </summary>
        private IAviVideoStream colorStream = null;

        /// <summary>
        /// AviWriter of depth data
        /// </summary>
        private AviWriter depthWriter = null;

        /// <summary>
        /// IAviVideoStream of depth data
        /// </summary>
        private IAviVideoStream depthStream = null;

        /// <summary>
        /// AviWriter of infrared data
        /// </summary>
        private AviWriter infraredWriter = null;

        /// <summary>
        /// IAviVideoStream of infrared data
        /// </summary>
        private IAviVideoStream infraredStream = null;

        /// <summary>
        /// AviWriter of shadow data
        /// </summary>
        private AviWriter shadowWriter = null;

        /// <summary>
        /// IAviVideoStream of shadow data
        /// </summary>
        private IAviVideoStream shadowStream = null;

        /// <summary>
        /// Size of the RGB pixel in the bitmap
        /// </summary>
        private readonly int bytesPerPixel = (PixelFormats.Bgra32.BitsPerPixel + 7) / 8;

        /// <summary>
        /// Active Kinect sensor
        /// </summary>
        private KinectSensor kinectSensor = null;

        /// <summary>
        /// Coordinate mapper to map one type of point to another
        /// </summary>
        private CoordinateMapper coordinateMapper = null;

        /// <summary>
        /// Frame reader of color
        /// </summary>
        private ColorFrameReader colorFrameReader = null;

        /// <summary>
        /// Frame reader of depth
        /// </summary>
        private DepthFrameReader depthFrameReader = null;

        /// <summary>
        /// Frame reader of Infrared
        /// </summary>
        private InfraredFrameReader infraredFrameReader = null;

        /// <summary>
        /// Frame reader of Bodies detected by Kinect
        /// </summary>
        private BodyFrameReader bodyFrameReader = null;

        /// <summary>
        /// Frame reader of Body Index detected by Kinect
        /// </summary>
        private BodyIndexFrameReader bodyIndexFrameReader = null;

        /// <summary>
        /// Bitmap to display shadow
        /// </summary>
        private WriteableBitmap bitmapShadow = null;

        ///  <sumary>
        ///  Bitmap to display color
        /// </summary>
        private WriteableBitmap bitmapColor = null;

        ///  <sumary>
        ///  Bitmap to display intensity
        /// </summary>
        private WriteableBitmap bitmapInfared = null;

        ///  <sumary>
        ///  Bitmap to display depth
        /// </summary>
        private WriteableBitmap bitmapDepth = null;

        ///  <sumary>
        ///  Matrix of bodies
        /// </summary>
        private Body[] bodies = null;

        /// <summary>
        /// Array of color pixels
        /// </summary>
        private byte[] colorPixels = null;

        /// <summary>
        /// Array of shadow data
        /// </summary>
        private byte[] shadowData = null;

        /// <summary>
        /// Array of shadow pixels used for the output
        /// </summary>
        private byte[] shadowPixels = null;

        /// <summary>
        /// Array of infrared data
        /// </summary>
        private ushort[] infraredData = null;

        /// <summary>
        /// Array of infrared pixels used for the output
        /// </summary>
        private byte[] infraredPixels = null;

        /// <summary>
        /// Array of depth data
        /// </summary>
        private ushort[] depthData = null;

        /// <summary>
        /// Array of depth pixels used for the output
        /// </summary>
        private byte[] depthPixels = null;

        /// <summary>
        /// The size in bytes of the bitmap back buffer
        /// </summary>
        private uint bitmapBackBufferSize = 0;

        /// <summary>
        /// Intermediate storage for the color to depth mapping
        /// </summary>
        private DepthSpacePoint[] colorMappedToDepthPoints = null;

        /// <summary>
        /// Current status text to display
        /// </summary>
        private string statusText = null;

        /// <summary>
        ///  Matrix of colors
        /// </summary>
        private System.Windows.Media.Color[] colors = null;

        /// <summary>
        /// definition of bones
        /// </summary>
        private List<Tuple<JointType, JointType>> bones;

        /// <summary>
        /// Drawing image that we will display
        /// </summary>
        private DrawingImage imageSource;

        /// <summary>
        /// Drawing group for body rendering output
        /// </summary>
        private DrawingGroup drawingGroup;

        /// <summary>
        /// List of colors for each body tracked
        /// </summary>
        private List<System.Windows.Media.Pen> bodyColors;

        /// <summary>
        /// Constant for clamping Z values of camera space points from being negative
        /// </summary>
        private const float InferredZPositionClamp = 0.1f;

        /// <summary>
        /// Brush used for drawing joints that are currently tracked
        /// </summary>
        private readonly System.Windows.Media.Brush trackedJointBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 68, 192, 68));

        /// <summary>
        /// Brush used for drawing joints that are currently inferred
        /// </summary>        
        private readonly System.Windows.Media.Brush inferredJointBrush = System.Windows.Media.Brushes.Yellow;

        /// <summary>
        /// Thickness of drawn joint lines
        /// </summary>
        private const double JointThickness = 3;

        /// <summary>
        /// Pen used for drawing bones that are currently inferred
        /// </summary>        
        private readonly System.Windows.Media.Pen inferredBonePen = new System.Windows.Media.Pen(System.Windows.Media.Brushes.Gray, 1);

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as closed
        /// </summary>
        private readonly System.Windows.Media.Brush handClosedBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 255, 0, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as opened
        /// </summary>
        private readonly System.Windows.Media.Brush handOpenBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 255, 0));

        /// <summary>
        /// Brush used for drawing hands that are currently tracked as in lasso (pointer) position
        /// </summary>
        private readonly System.Windows.Media.Brush handLassoBrush = new SolidColorBrush(System.Windows.Media.Color.FromArgb(128, 0, 0, 255));

        /// <summary>
        /// Radius of drawn hand circles
        /// </summary>
        private const double HandSize = 30;

        /// <summary>
        /// Bool usted to know if save RGB images
        /// </summary>
        private bool saveRGB = false;

        /// <summary>
        /// Bool usted to know if save Depth images
        /// </summary>
        private bool saveDepth = false;

        /// <summary>
        /// Bool usted to know if save Infrared images
        /// </summary>
        private bool saveInfrared = false;

        /// <summary>
        /// Bool usted to know if save Shadow images
        /// </summary>
        private bool saveShadow = false;

        /// <summary>
        /// Bool usted to know if save Skeleton images
        /// </summary>
        private bool saveSkeleton = false;

        /// <summary>
        /// Bool usted to know if save XML files
        /// </summary>
        private bool saveXML = false;
        
        /// <summary>
        /// Bool used to know if save
        /// </summary>
        private bool save = false;

        /// <summary>
        /// Base directory recordings
        /// </summary>
        private String base_directory = "";

        /// <summary>
        /// Base name recordings
        /// </summary>
        private String actorName = "";

        /// <summary>
        /// Width of depth frame
        /// </summary>
        private int depthWidth;

        /// <summary>
        /// Height of depth frame
        /// </summary>
        private int depthHeight;

        /// <summary>
        /// Width of color frame
        /// </summary>
        private int colorWidth;

        /// <summary>
        /// Height of color frame
        /// </summary>
        private int colorHeight;


        /// <summary>
        /// Initializes a new instance of the MainWindow class.
        /// </summary>
        public MainWindow()
        {
            tcs = new TaskCompletionSource<bool>();
            tcs.SetResult(true);

            this.kinectSensor = KinectSensor.GetDefault();

            this.colorFrameReader = this.kinectSensor.ColorFrameSource.OpenReader();
            this.depthFrameReader = this.kinectSensor.DepthFrameSource.OpenReader();
            this.infraredFrameReader = this.kinectSensor.InfraredFrameSource.OpenReader();
            this.bodyIndexFrameReader = this.kinectSensor.BodyIndexFrameSource.OpenReader();
            this.bodyFrameReader = this.kinectSensor.BodyFrameSource.OpenReader();

            this.colorFrameReader.FrameArrived += this.Reader_ColorFrameArrived;
            this.depthFrameReader.FrameArrived += this.Reader_DepthFrameArrived;
            this.infraredFrameReader.FrameArrived += this.Reader_InfraredFrameArrived;
            this.bodyIndexFrameReader.FrameArrived += this.Reader_BodyIndexFrameArriver;
            this.bodyFrameReader.FrameArrived += this.Reader_BodyFrameArrived;

            this.coordinateMapper = this.kinectSensor.CoordinateMapper;

            FrameDescription depthFrameDescription = this.kinectSensor.DepthFrameSource.FrameDescription;

            depthWidth = depthFrameDescription.Width;
            depthHeight = depthFrameDescription.Height;

            FrameDescription colorFrameDescription = this.kinectSensor.ColorFrameSource.FrameDescription;

            colorWidth = colorFrameDescription.Width;
            colorHeight = colorFrameDescription.Height;

            FrameDescription infaredFrameDescription = this.kinectSensor.InfraredFrameSource.FrameDescription;

            int infaredWidth = infaredFrameDescription.Width;
            int infaredHeight = infaredFrameDescription.Height;

            FrameDescription shadowFrameDescription = this.kinectSensor.BodyIndexFrameSource.FrameDescription;
            int shadowWidth = shadowFrameDescription.Width;
            int shadowHeight = shadowFrameDescription.Height;

            this.colorMappedToDepthPoints = new DepthSpacePoint[colorWidth * colorHeight];
            this.colorPixels = new byte[colorWidth * colorHeight * this.bytesPerPixel];
            this.infraredData = new ushort[infaredWidth * infaredHeight];
            this.infraredPixels = new byte[infaredWidth * infaredHeight * this.bytesPerPixel];
            this.depthData = new ushort[depthWidth * depthHeight];
            this.depthPixels = new byte[depthWidth * depthHeight * this.bytesPerPixel];
            this.shadowData = new byte[shadowWidth * shadowHeight];
            this.shadowPixels = new byte[shadowWidth * shadowHeight * this.bytesPerPixel];
            this.bodies = new Body[this.kinectSensor.BodyFrameSource.BodyCount];

            this.bitmapShadow = new WriteableBitmap(shadowWidth, shadowHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
            this.bitmapColor = new WriteableBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Bgra32, null);
            this.bitmapInfared = new WriteableBitmap(infaredWidth, infaredHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
            this.bitmapDepth = new WriteableBitmap(depthWidth, depthHeight,  96.0, 96.0, PixelFormats.Bgr32, null);

            //Set colors
            this.colors = new System.Windows.Media.Color[6];
            this.colors[0] = System.Windows.Media.Color.FromRgb(255, 0, 0);
            this.colors[1] = System.Windows.Media.Color.FromRgb(0, 255, 0);
            this.colors[2] = System.Windows.Media.Color.FromRgb(0, 0, 255);
            this.colors[3] = System.Windows.Media.Color.FromRgb(255, 255, 0);
            this.colors[4] = System.Windows.Media.Color.FromRgb(255, 0, 255);
            this.colors[5] = System.Windows.Media.Color.FromRgb(0, 255, 255);

            // populate body colors, one for each BodyIndex
            this.bodyColors = new List<System.Windows.Media.Pen>();

            this.bodyColors.Add(new System.Windows.Media.Pen(System.Windows.Media.Brushes.Red, 6));
            this.bodyColors.Add(new System.Windows.Media.Pen(System.Windows.Media.Brushes.Orange, 6));
            this.bodyColors.Add(new System.Windows.Media.Pen(System.Windows.Media.Brushes.Green, 6));
            this.bodyColors.Add(new System.Windows.Media.Pen(System.Windows.Media.Brushes.Blue, 6));
            this.bodyColors.Add(new System.Windows.Media.Pen(System.Windows.Media.Brushes.Indigo, 6));
            this.bodyColors.Add(new System.Windows.Media.Pen(System.Windows.Media.Brushes.Violet, 6));

            // Create the drawing group we'll use for drawing
            this.drawingGroup = new DrawingGroup();

            // Create an image source that we can use in our image control
            this.imageSource = new DrawingImage(this.drawingGroup);

            // use the window object as the view model in this simple example
            this.DataContext = this;

            // a bone defined as a line between two joints
            this.bones = new List<Tuple<JointType, JointType>>();

            // Torso
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Head, JointType.Neck));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.Neck, JointType.SpineShoulder));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.SpineMid));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineMid, JointType.SpineBase));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineShoulder, JointType.ShoulderLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.SpineBase, JointType.HipLeft));

            // Right Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderRight, JointType.ElbowRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowRight, JointType.WristRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.HandRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandRight, JointType.HandTipRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristRight, JointType.ThumbRight));

            // Left Arm
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ShoulderLeft, JointType.ElbowLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.ElbowLeft, JointType.WristLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.HandLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HandLeft, JointType.HandTipLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.WristLeft, JointType.ThumbLeft));

            // Right Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipRight, JointType.KneeRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeRight, JointType.AnkleRight));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleRight, JointType.FootRight));

            // Left Leg
            this.bones.Add(new Tuple<JointType, JointType>(JointType.HipLeft, JointType.KneeLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.KneeLeft, JointType.AnkleLeft));
            this.bones.Add(new Tuple<JointType, JointType>(JointType.AnkleLeft, JointType.FootLeft));


            // Calculate the WriteableBitmap back buffer size
            this.bitmapBackBufferSize = (uint)((this.bitmapShadow.BackBufferStride * (this.bitmapShadow.PixelHeight - 1)) + (this.bitmapShadow.PixelWidth * this.bytesPerPixel));

            this.kinectSensor.IsAvailableChanged += this.Sensor_IsAvailableChanged;

            this.kinectSensor.Open();

            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.NoSensorStatusText;

            this.InitializeComponent();
        }

        /// <summary>
        /// INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the bitmapShadow to display
        /// </summary>
        public ImageSource ImageSourceShadow
        {
            get
            {
                return this.bitmapShadow;
            }
        }

        /// <summary>
        /// Gets the bitmapColor to display
        /// </summary>
        public ImageSource ImageSourceColor
        {
            get
            {
                return this.bitmapColor;
            }
        }

        /// <summary>
        /// Gets the bitmapColor to display
        /// </summary>
        public ImageSource ImageSourceInfared
        {
            get
            {
                return this.bitmapInfared;
            }
        }

        /// <summary>
        /// Gets the bitmapDepth to display
        /// </summary>
        public ImageSource ImageSourceDepth
        {
            get
            {
                return this.bitmapDepth;
            }
        }

        /// <summary>
        /// Gets the bitmapSkeleton to display
        /// </summary>
        public ImageSource ImageSourceSkeleton
        {
            get
            {
                return this.imageSource;
            }
        }

        /// <summary>
        /// Gets or sets the current status text to display
        /// </summary>
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }

        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (this.colorFrameReader != null)
            {
                this.colorFrameReader.Dispose();
                this.colorFrameReader = null;
            }

            if (this.depthFrameReader != null)
            {
                this.depthFrameReader.Dispose();
                this.depthFrameReader = null;
            }

            if (this.infraredFrameReader != null)
            {
                this.infraredFrameReader.Dispose();
                this.infraredFrameReader = null;
            }

            if (this.bodyFrameReader != null)
            {
                this.bodyFrameReader.Dispose();
                this.bodyFrameReader = null;
            }

            if (this.bodyIndexFrameReader != null)
            {
                this.bodyIndexFrameReader.Dispose();
                this.bodyIndexFrameReader = null;
            }

            if (this.kinectSensor != null)
            {
                this.kinectSensor.Close();
                this.kinectSensor = null;
            }
        }

        /// <summary>
        /// Show the dialog for select the directory of the recording
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void selectDirectory(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                base_directory = folderBrowserDialog1.SelectedPath;
            }
        }

        /// <summary>
        /// Handles the user clicking on the save button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void ScreenshotButton_Click(object sender, RoutedEventArgs e)
        {
            actorName = textBoxBaseName.Text;

            if (actorName == "")
            {
                this.StatusText = "Please, enter a name.";
                string message = "Please, enter a name.";
                string caption = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                System.Windows.Forms.MessageBox.Show(message, caption, buttons);
                return;
            }

            if (base_directory == "")
            {
                this.StatusText = "Please, enter a directory.";
                string message = "Please, enter a directory.";
                string caption = "Error";
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                System.Windows.Forms.MessageBox.Show(message, caption, buttons);
                return;
            }

            this.save = !this.save;
            this.RGBCheckBox.IsEnabled = !this.RGBCheckBox.IsEnabled;
            this.DepthCheckBox.IsEnabled = !this.DepthCheckBox.IsEnabled;
            this.InfraredCheckBox.IsEnabled = !this.InfraredCheckBox.IsEnabled;
            this.ShadowCheckBox.IsEnabled = !this.ShadowCheckBox.IsEnabled;
            this.SkeletonImageCheckBox.IsEnabled = !this.SkeletonImageCheckBox.IsEnabled;
            this.XMLskeletonCheckBox.IsEnabled = !this.XMLskeletonCheckBox.IsEnabled;

            if(this.save){
                this.saveRGB = this.RGBCheckBox.IsChecked.Value;
                this.saveDepth = this.DepthCheckBox.IsChecked.Value;
                this.saveInfrared = this.InfraredCheckBox.IsChecked.Value;
                this.saveShadow = this.ShadowCheckBox.IsChecked.Value;
                this.saveSkeleton = this.SkeletonImageCheckBox.IsChecked.Value;
                this.saveXML = this.XMLskeletonCheckBox.IsChecked.Value;

                createFolder(base_directory + "\\RecogidaDatos\\Results");
                createFolder(base_directory + "\\RecogidaDatos\\truth");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\videos\\depth");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\videos\\esqueleto");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\videos\\rgb");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\videos\\infrared");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\videos\\shadow");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\rgb");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\depth\\xml");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\esqueleto");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\infrared");
                createFolder(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\shadow");

                if(this.saveRGB){
                    colorTasks = new List<Task>();
                    Mpeg4VideoEncoderVcm colorEncoder = new Mpeg4VideoEncoderVcm(colorWidth, colorHeight,
                                    frame_rate, // frame rate
                                    0, // number of frames, if known beforehand, or zero
                                    quality, // quality, though usually ignored :(
                                    KnownFourCCs.Codecs.X264 // codecs preference
                                    );

                    colorWriter = new AviWriter(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\rgb\\" + actorName + "_RGB.avi")
                    {
                        FramesPerSecond = (decimal)frame_rate,
                        // Emitting AVI v1 index in addition to OpenDML index (AVI v2)
                        // improves compatibility with some software, including 
                        // standard Windows programs like Media Player and File Explorer
                        EmitIndex1 = true
                    };
                    colorStream = colorWriter.AddEncodingVideoStream(colorEncoder,true, width: colorWidth, height: colorHeight);
                }

                if(this.saveDepth){
                    depthTasks = new List<Task>();
                    Mpeg4VideoEncoderVcm depthEncoder = new Mpeg4VideoEncoderVcm(depthWidth, depthHeight,
                                    frame_rate, // frame rate
                                    0, // number of frames, if known beforehand, or zero
                                    quality, // quality, though usually ignored :(
                                    KnownFourCCs.Codecs.X264 // codecs preference
                                    );

                    depthWriter = new AviWriter(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\depth\\" + actorName + "_Depth.avi")
                    {
                        FramesPerSecond = (decimal)frame_rate,
                        // Emitting AVI v1 index in addition to OpenDML index (AVI v2)
                        // improves compatibility with some software, including 
                        // standard Windows programs like Media Player and File Explorer
                        EmitIndex1 = true
                    };
                    depthStream = depthWriter.AddEncodingVideoStream(depthEncoder, true, width: depthWidth, height: depthHeight);
                }

                if(this.saveInfrared){
                    infraredTasks = new List<Task>();
                    Mpeg4VideoEncoderVcm infraredEncoder = new Mpeg4VideoEncoderVcm(depthWidth, depthHeight,
                                    frame_rate, // frame rate
                                    0, // number of frames, if known beforehand, or zero
                                    quality, // quality, though usually ignored :(
                                    KnownFourCCs.Codecs.X264 // codecs preference
                                    );

                    infraredWriter = new AviWriter(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\infrared\\" + actorName + "_Infrared.avi")
                    {
                        FramesPerSecond = (decimal)frame_rate,
                        // Emitting AVI v1 index in addition to OpenDML index (AVI v2)
                        // improves compatibility with some software, including 
                        // standard Windows programs like Media Player and File Explorer
                        EmitIndex1 = true
                    };
                    infraredStream = infraredWriter.AddEncodingVideoStream(infraredEncoder, true, width: depthWidth, height: depthHeight);
                }

                if(this.saveShadow){
                    shadowTasks = new List<Task>();
                    Mpeg4VideoEncoderVcm shadowEncoder = new Mpeg4VideoEncoderVcm(depthWidth, depthHeight,
                                    frame_rate, // frame rate
                                    0, // number of frames, if known beforehand, or zero
                                    quality, // quality, though usually ignored :(
                                    KnownFourCCs.Codecs.X264 // codecs preference
                                    );

                    shadowWriter = new AviWriter(base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\shadow\\" + actorName + "_Shadow.avi")
                    {
                        FramesPerSecond = (decimal)frame_rate,
                        // Emitting AVI v1 index in addition to OpenDML index (AVI v2)
                        // improves compatibility with some software, including 
                        // standard Windows programs like Media Player and File Explorer
                        EmitIndex1 = true
                    };
                    shadowStream = shadowWriter.AddEncodingVideoStream(shadowEncoder, true, width: depthWidth, height: depthHeight);
                }

                if(this.saveXML){
                    xmlTasks = new List<Task>();
                }
            }
            else
            {
                if(colorWriter != null){
                    this.StatusText = "Saving RGB video...";
                    colorWriter.Close();
                    colorWriter = null;
                    foreach(Task task in colorTasks){
                        task.Wait();
                    }
                    colorTasks = null;
                    this.StatusText = "...RGB video saved!!";
                }

                if(depthWriter != null){
                    this.StatusText = "Saving Depth video...";
                    depthWriter.Close();
                    depthWriter = null;
                    foreach (Task task in depthTasks)
                    {
                        task.Wait();
                    }
                    depthTasks = null;
                    this.StatusText = "...Depth video saved!!";
                }

                if(infraredWriter != null){
                    this.StatusText = "Saving Infrared video...";
                    infraredWriter.Close();
                    infraredWriter = null;
                    foreach (Task task in infraredTasks)
                    {
                        task.Wait();
                    }
                    infraredTasks = null;
                    this.StatusText = "...Infrared video saved!!";
                }

                if(shadowWriter != null){
                    this.StatusText = "Saving Shadow video...";
                    shadowWriter.Close();
                    shadowWriter = null;
                    foreach (Task task in shadowTasks)
                    {
                        task.Wait();
                    }
                    shadowTasks = null;
                    this.StatusText = "...Shadow video saved!!";
                }

                if(xmlTasks != null){
                    this.StatusText = "Saving XML files...";
                    foreach (Task task in xmlTasks)
                    {
                        task.Wait();
                    }
                    xmlTasks = null;
                    this.StatusText = "...XML files saved!!";
                }
            }
        }

        /// <summary>
        /// Create a new folder
        /// </summary>
        /// <param name="path">Path of the new folder </param>
        private void createFolder(String path)
        {
            try
            {
                // Determine whether the directory exists. 
                if (Directory.Exists(path))
                {
                    Console.WriteLine("That path exists already.");
                    return;
                }

                // Try to create the directory.
                DirectoryInfo di = Directory.CreateDirectory(path);
                Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path));
            }
            catch (Exception exception)
            {
                Console.WriteLine("The process failed: {0}", exception.ToString());
            }
            finally { }
        }

        /// <summary>
        /// Process depth and color to make Shadow frame
        /// </summary>
        /// <param name="depthFrame">Depth Frame</param>
        /// <param name="colorFrame">Color Frame</param>
        /// <param name="bodyIndexFrame">Body Index Frame</param>
        private void ProcessingShadow(BodyIndexFrame bodyIndexFrame)
        {
            using (bodyIndexFrame)
            {
                // Get the description
                FrameDescription frameDesc = bodyIndexFrame.FrameDescription;

                if (((frameDesc.Width * frameDesc.Height) == shadowData.Length) && (frameDesc.Width == bitmapShadow.PixelWidth) && (frameDesc.Height == bitmapShadow.PixelHeight))
                {
                    this.bitmapShadow.Lock();
                    bool isBitmapLocked = true;

                    // Copy data
                    bodyIndexFrame.CopyFrameDataToArray(shadowData);
                    
                    int index = 0;

                    for (int i = 0; i < shadowData.Length; i++ )
                    {
                        //Get pixel user data
                        byte pixelUser = shadowData[i];

                        //Asign color
                        if (pixelUser < 6) 
                        {
                            shadowPixels[index++] = this.colors[pixelUser].B;
                            shadowPixels[index++] = this.colors[pixelUser].G;
                            shadowPixels[index++] = this.colors[pixelUser].R;
                        }
                        else
                        {
                            shadowPixels[index++] = 0;
                            shadowPixels[index++] = 0;
                            shadowPixels[index++] = 0;
                        }
                        
                        ++index;
                    }

                    // Copy output to bitmap
                    bitmapShadow.WritePixels(
                            new Int32Rect(0, 0, frameDesc.Width, frameDesc.Height),
                            shadowPixels,
                            frameDesc.Width * bytesPerPixel,
                            0);

                    if (isBitmapLocked)
                    {
                        this.bitmapShadow.Unlock();
                    }

                }
            }
        }

        /// <summary>
        /// Process color to make color frame
        /// </summary>
        /// <param name="colorFrame">Color Frame</param>
        private void ProcessingColor(ColorFrame colorFrame)
        {
            using (colorFrame)
            {
                // Get frame description
                FrameDescription frameDesc = colorFrame.FrameDescription;

                // Check if width/height matches
                if (frameDesc.Width == bitmapColor.PixelWidth && frameDesc.Height == bitmapColor.PixelHeight)
                {
                    // Lock the bitmap for writing
                    this.bitmapColor.Lock();
                    bool isBitmapLocked = true;

                    // Copy data to array based on image format
                    if (colorFrame.RawColorImageFormat == ColorImageFormat.Bgra)
                    {
                        colorFrame.CopyRawFrameDataToArray(colorPixels);
                    }
                    else colorFrame.CopyConvertedFrameDataToArray(colorPixels, ColorImageFormat.Bgra);

                    // Copy output to bitmap
                    bitmapColor.WritePixels(
                            new Int32Rect(0, 0, frameDesc.Width, frameDesc.Height),
                            colorPixels,
                            frameDesc.Width * bytesPerPixel,
                            0);

                    if (isBitmapLocked)
                    {
                        this.bitmapColor.Unlock();
                    }
                }
            }
        }

        /// <summary>
        /// Process infared to make infared frame
        /// </summary>
        /// <param name="infaredFrame">Infared Frame</param>
        private void ProcessingInfared(InfraredFrame infaredFrame)
        {
            // Process it
            using (infaredFrame)
            {
                // Get the description
                FrameDescription frameDesc = infaredFrame.FrameDescription;

                if (((frameDesc.Width * frameDesc.Height) == infraredData.Length) && (frameDesc.Width == bitmapInfared.PixelWidth) && (frameDesc.Height == bitmapInfared.PixelHeight))
                {
                    this.bitmapInfared.Lock();
                    bool isBitmapLocked = true;

                    // Copy data
                    infaredFrame.CopyFrameDataToArray(infraredData);

                    int colorPixelIndex = 0;

                    for (int i = 0; i < infraredData.Length; ++i)
                    {
                        // Get infrared value
                        ushort ir = infraredData[i];

                        // Bitshift
                        byte intensity = (byte)(ir >> 8);

                        // Assign infrared intensity
                        infraredPixels[colorPixelIndex++] = intensity;
                        infraredPixels[colorPixelIndex++] = intensity;
                        infraredPixels[colorPixelIndex++] = intensity;

                        ++colorPixelIndex;
                    }

                    // Copy output to bitmap
                    bitmapInfared.WritePixels(
                            new Int32Rect(0, 0, frameDesc.Width, frameDesc.Height),
                            infraredPixels,
                            frameDesc.Width * bytesPerPixel,
                            0);

                    if (isBitmapLocked)
                    {
                        this.bitmapInfared.Unlock();
                    }
                }
            }
        }


        /// <summary>
        /// Create and save the xml files of the tracked users
        /// </summary>
        /// <param name="bodies">Bodies detected by the Kinect.</param>
        /// <param name="timestamp">Timestamp of the frame</param>
        private void ProcessingXML(Body[] bodies, double timestamp)
        {
            String xmlPath = base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\depth\\xml\\" + actorName + "_"+ timestamp + "_depth.xml";
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("frame");
            XmlElement times = doc.CreateElement("timestamp");
            times.InnerText = timestamp.ToString();
            XmlElement users = doc.CreateElement("users");

            foreach (Body body in bodies)
            {
                if(body.IsTracked){
                    XmlElement user = doc.CreateElement("user");
                    XmlElement id = doc.CreateElement("id");
                    id.InnerText = body.TrackingId.ToString();
                    XmlElement IsRestricted = doc.CreateElement("IsRestricted");
                    IsRestricted.InnerText = body.IsRestricted.ToString();
                    XmlElement IsTracked = doc.CreateElement("IsTracked");
                    IsTracked.InnerText = body.IsTracked.ToString();
                    XmlElement ClippedEdges = doc.CreateElement("ClippedEdges");
                    ClippedEdges.InnerText = body.ClippedEdges.ToString();


                    XmlElement joints = doc.CreateElement("joints");

                    IReadOnlyDictionary<JointType, Joint> joints_dictionary = body.Joints;
                    foreach (JointType jointType in joints_dictionary.Keys)
                    {

                        XmlElement j = doc.CreateElement(jointType.ToString());

                        //Position
                        XmlElement j_position = doc.CreateElement("position");
                        XmlElement j_position_x = doc.CreateElement("positionx");
                        XmlElement j_position_y = doc.CreateElement("positiony");
                        XmlElement j_position_z = doc.CreateElement("positionz");
                        XmlElement j_position_TrackingState = doc.CreateElement("TrackingState");

                        //orientation
                        XmlElement j_orientation = doc.CreateElement("orientation");
                        XmlElement j_orientation_x = doc.CreateElement("orientationx");
                        XmlElement j_orientation_y = doc.CreateElement("orientationy");
                        XmlElement j_orientation_z = doc.CreateElement("orientationz");
                        XmlElement j_orientation_w = doc.CreateElement("orientationw");


                        //Data
                        j_position_x.InnerText = body.Joints[jointType].Position.X.ToString();
                        j_position_y.InnerText = body.Joints[jointType].Position.Y.ToString();
                        j_position_z.InnerText = body.Joints[jointType].Position.Z.ToString();
                        j_position_TrackingState.InnerText = body.Joints[jointType].TrackingState.ToString();

                        j_orientation_x.InnerText = body.JointOrientations[jointType].Orientation.X.ToString();
                        j_orientation_y.InnerText = body.JointOrientations[jointType].Orientation.Y.ToString();
                        j_orientation_z.InnerText = body.JointOrientations[jointType].Orientation.Z.ToString();
                        j_orientation_w.InnerText = body.JointOrientations[jointType].Orientation.W.ToString();


                        //XML Tree
                        j.AppendChild(j_position);
                        j_position.AppendChild(j_position_x);
                        j_position.AppendChild(j_position_y);
                        j_position.AppendChild(j_position_z);
                        j_position.AppendChild(j_position_TrackingState);

                        j.AppendChild(j_orientation);
                        j_orientation.AppendChild(j_orientation_x);
                        j_orientation.AppendChild(j_orientation_y);
                        j_orientation.AppendChild(j_orientation_z);
                        j_orientation.AppendChild(j_orientation_w);


                        joints.AppendChild(j);
                    }

                    XmlElement HandLeftState = doc.CreateElement("HandLeftState");
                    HandLeftState.InnerText = body.HandLeftState.ToString();
                    XmlElement HandLeftConfidence = doc.CreateElement("HandLeftConfidence");
                    HandLeftConfidence.InnerText = body.HandLeftConfidence.ToString();
                    XmlElement HandRightState = doc.CreateElement("HandRightState");
                    HandRightState.InnerText = body.HandRightState.ToString();
                    XmlElement HandRightConfidence = doc.CreateElement("HandRightConfidence");
                    HandRightConfidence.InnerText = body.HandRightConfidence.ToString();


                    XmlElement Lean = doc.CreateElement("Lean");
                    XmlElement Lean_x = doc.CreateElement("Lean_x");
                    Lean_x.InnerText = body.Lean.X.ToString();
                    XmlElement Lean_y = doc.CreateElement("Lean_y");
                    Lean_y.InnerText = body.Lean.Y.ToString();
                    Lean.AppendChild(Lean_x);
                    Lean.AppendChild(Lean_y);
                    XmlElement LeanTrackingState = doc.CreateElement("LeanTrackingState");
                    LeanTrackingState.InnerText = body.LeanTrackingState.ToString();


                    user.AppendChild(id);
                    user.AppendChild(IsRestricted);
                    user.AppendChild(IsTracked);
                    user.AppendChild(ClippedEdges);
                    user.AppendChild(joints);
                    user.AppendChild(HandLeftState);
                    user.AppendChild(HandLeftConfidence);
                    user.AppendChild(HandRightState);
                    user.AppendChild(HandRightConfidence);
                    user.AppendChild(Lean);
                    user.AppendChild(LeanTrackingState);
                    users.AppendChild(user);
                }
            }

            root.AppendChild(times);
            root.AppendChild(users);
            doc.AppendChild(root);

            doc.Save(xmlPath);
        }

        /// <summary>
        /// Create and save a new empty XML file when no users are detected
        /// </summary>
        /// <param name="timestamp">Timestamp of the frame</param>
        private void AddEmptyXMLfile(double timestamp)
        {

            String xmlPath = base_directory + "\\RecogidaDatos\\videos\\actores\\" + actorName + "\\depth\\xml\\" + actorName + "_" + timestamp + "_depth.xml";
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("frame");
            XmlElement times = doc.CreateElement("timestamp");
            times.InnerText = timestamp.ToString();
            XmlElement users = doc.CreateElement("users");

            root.AppendChild(times);
            root.AppendChild(users);
            doc.AppendChild(root);

            doc.Save(xmlPath);
        }

        /// <summary>
        /// Checks if there are any users tracked
        /// </summary>
        /// <param name="bodies">Bodies detected by the Kinect.</param>
        private bool anyTracked(Body[] bodies)
        {
            foreach (Body body in bodies)
            {
                if(body.IsTracked){
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Process the BodyFrame captured by the Kinect
        /// </summary>
        /// <param name="bodyFrame">BodyFrame captured by the Kinect.</param>
        /// <param name="timestamp">Timestamp of the frame</param>
        private void ProcessingBodies(BodyFrame bodyFrame, double timestamp)
        {
            bool dataReceived = false;

            using (bodyFrame)
            {
                if (bodyFrame != null)
                {
                    if (this.bodies == null)
                    {
                        this.bodies = new Body[bodyFrame.BodyCount];
                    }

                    // The first time GetAndRefreshBodyData is called, Kinect will allocate each Body in the array.
                    // As long as those body objects are not disposed and not set to null in the array,
                    // those body objects will be re-used.
                    bodyFrame.GetAndRefreshBodyData(this.bodies);
                    dataReceived = true;
                    if (this.save && this.saveXML)
                    {
                        if (anyTracked(bodies))
                        {
                            Task task = Task.Run(() => ProcessingXML((Body[])this.bodies.Clone(), timestamp));
                            xmlTasks.Add(task);
                        }
                        else
                        {
                            //String XMLdata = "<frame><timestamp>" + timestamp + "</timestamp><users/></frame>";
                            Task task = Task.Run(() => AddEmptyXMLfile(timestamp));
                            xmlTasks.Add(task);
                        }
                    }
                }
            }

            if (dataReceived)
            {
                using (DrawingContext dc = this.drawingGroup.Open())
                {
                    // Draw a transparent background to set the render size
                    dc.DrawRectangle(System.Windows.Media.Brushes.Black, null, new Rect(0.0, 0.0, this.depthWidth, this.depthHeight));

                    int penIndex = 0;
                    foreach (Body body in this.bodies)
                    {
                        System.Windows.Media.Pen drawPen = this.bodyColors[penIndex++];

                        if (body.IsTracked)
                        {
                            IReadOnlyDictionary<JointType, Joint> joints = body.Joints;

                            // convert the joint points to depth (display) space
                            Dictionary<JointType, System.Windows.Point> jointPoints = new Dictionary<JointType, System.Windows.Point>();

                            foreach (JointType jointType in joints.Keys)
                            {
                                // sometimes the depth(Z) of an inferred joint may show as negative
                                // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                                CameraSpacePoint position = joints[jointType].Position;
                                if (position.Z < 0)
                                {
                                    position.Z = InferredZPositionClamp;
                                }

                                DepthSpacePoint depthSpacePoint = this.coordinateMapper.MapCameraPointToDepthSpace(position);
                                jointPoints[jointType] = new System.Windows.Point(depthSpacePoint.X, depthSpacePoint.Y);
                            }

                            this.DrawBody(joints, jointPoints, dc, drawPen);

                            this.DrawHand(body.HandLeftState, jointPoints[JointType.HandLeft], dc);
                            this.DrawHand(body.HandRightState, jointPoints[JointType.HandRight], dc);
                        }
                    }

                    // prevent drawing outside of our render area
                    this.drawingGroup.ClipGeometry = new RectangleGeometry(new Rect(0.0, 0.0, this.depthWidth, this.depthHeight));
                }
            }
        }

        /// <summary>
        /// Draws a body
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// <param name="drawingPen">specifies color to draw a specific body</param>
        private void DrawBody(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, System.Windows.Point> jointPoints, DrawingContext drawingContext, System.Windows.Media.Pen drawingPen)
        {
            // Draw the bones
            foreach (var bone in this.bones)
            {
                this.DrawBone(joints, jointPoints, bone.Item1, bone.Item2, drawingContext, drawingPen);
            }

            // Draw the joints
            foreach (JointType jointType in joints.Keys)
            {
                System.Windows.Media.Brush drawBrush = null;

                TrackingState trackingState = joints[jointType].TrackingState;

                if (trackingState == TrackingState.Tracked)
                {
                    drawBrush = this.trackedJointBrush;
                }
                else if (trackingState == TrackingState.Inferred)
                {
                    drawBrush = this.inferredJointBrush;
                }

                if (drawBrush != null)
                {
                    drawingContext.DrawEllipse(drawBrush, null, jointPoints[jointType], JointThickness, JointThickness);
                }
            }
        }

        /// <summary>
        /// Draws one bone of a body (joint to joint)
        /// </summary>
        /// <param name="joints">joints to draw</param>
        /// <param name="jointPoints">translated positions of joints to draw</param>
        /// <param name="jointType0">first joint of bone to draw</param>
        /// <param name="jointType1">second joint of bone to draw</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        /// /// <param name="drawingPen">specifies color to draw a specific bone</param>
        private void DrawBone(IReadOnlyDictionary<JointType, Joint> joints, IDictionary<JointType, System.Windows.Point> jointPoints, JointType jointType0, JointType jointType1, DrawingContext drawingContext, System.Windows.Media.Pen drawingPen)
        {
            Joint joint0 = joints[jointType0];
            Joint joint1 = joints[jointType1];

            // If we can't find either of these joints, exit
            if (joint0.TrackingState == TrackingState.NotTracked ||
                joint1.TrackingState == TrackingState.NotTracked)
            {
                return;
            }

            // We assume all drawn bones are inferred unless BOTH joints are tracked
            System.Windows.Media.Pen drawPen = this.inferredBonePen;
            if ((joint0.TrackingState == TrackingState.Tracked) && (joint1.TrackingState == TrackingState.Tracked))
            {
                drawPen = drawingPen;
            }

            drawingContext.DrawLine(drawPen, jointPoints[jointType0], jointPoints[jointType1]);
        }

        /// <summary>
        /// Draws a hand symbol if the hand is tracked: red circle = closed, green circle = opened; blue circle = lasso
        /// </summary>
        /// <param name="handState">state of the hand</param>
        /// <param name="handPosition">position of the hand</param>
        /// <param name="drawingContext">drawing context to draw to</param>
        private void DrawHand(HandState handState, System.Windows.Point handPosition, DrawingContext drawingContext)
        {
            switch (handState)
            {
                case HandState.Closed:
                    drawingContext.DrawEllipse(this.handClosedBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Open:
                    drawingContext.DrawEllipse(this.handOpenBrush, null, handPosition, HandSize, HandSize);
                    break;

                case HandState.Lasso:
                    drawingContext.DrawEllipse(this.handLassoBrush, null, handPosition, HandSize, HandSize);
                    break;
            }
        }

        /// <summary>
        /// Process the DepthFrame captured by the Kinect
        /// </summary>
        /// <param name="bodyFrame">DepthFrame captured by the Kinect.</param>
        private void ProcessingDepth(DepthFrame depthFrame)
        {
            using (depthFrame)
            {
                FrameDescription frameDesc = depthFrame.FrameDescription;

                if (((frameDesc.Width * frameDesc.Height) == depthData.Length) && (frameDesc.Width == bitmapDepth.PixelWidth) && (frameDesc.Height == bitmapDepth.PixelHeight))
                {
                    // Copy depth frames
                    depthFrame.CopyFrameDataToArray(depthData);

                    // Get min & max depth
                    ushort minDepth = depthFrame.DepthMinReliableDistance;
                    ushort maxDepth = depthFrame.DepthMaxReliableDistance;

                    // Adjust visualisation
                    int colorPixelIndex = 0;
                    for (int i = 0; i < depthData.Length; ++i)
                    {
                        // Get depth value
                        ushort depth = depthData[i];

                        if (depth == 0)
                        {
                            //Yellow
                            depthPixels[colorPixelIndex++] = 41;
                            depthPixels[colorPixelIndex++] = 239;
                            depthPixels[colorPixelIndex++] = 242;
                        }
                        else if (depth < minDepth || depth > maxDepth)
                        {
                            //Red
                            depthPixels[colorPixelIndex++] = 25;
                            depthPixels[colorPixelIndex++] = 0;
                            depthPixels[colorPixelIndex++] = 255;
                        }
                        else
                        {
                            double gray = (Math.Floor((double)depth / 250) * 12.75);

                            depthPixels[colorPixelIndex++] = (byte)gray;
                            depthPixels[colorPixelIndex++] = (byte)gray;
                            depthPixels[colorPixelIndex++] = (byte)gray;
                        }

                        // Increment
                        ++colorPixelIndex;
                    }

                    // Copy output to bitmap
                    bitmapDepth.WritePixels(
                            new Int32Rect(0, 0, frameDesc.Width, frameDesc.Height),
                            depthPixels,
                            frameDesc.Width * this.bytesPerPixel,
                            0);
                }
            }
        }

        /// <summary>
        /// Receives and processes a new ColorFrame captured by the Kinect.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_ColorFrameArrived(object sender, ColorFrameArrivedEventArgs e)
        {
            ColorFrame colorFrame = e.FrameReference.AcquireFrame();
           
            // If the Frame has expired by the time we process this event, return.
            if (colorFrame == null)
            { 
                return;
            }

            // We use a try/finally to ensure that we clean up before we exit the function.  
            // This includes calling Dispose on any Frame objects that we may have and unlocking the bitmap back buffer.
            try
            {
                ProcessingColor(colorFrame);

                if (this.save & this.saveRGB)
                {
                    colorTasks.Add(colorStream.WriteFrameAsync(true, (byte[])colorPixels.Clone(), 0, colorPixels.Length));
                }
            }
            finally
            {
                if (colorFrame != null)
                {
                    colorFrame.Dispose();
                }
            }
        }

        /// <summary>
        /// Receives and processes a new DepthFrame captured by the Kinect.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_DepthFrameArrived(object sender, DepthFrameArrivedEventArgs e)
        {
            DepthFrame depthFrame = e.FrameReference.AcquireFrame();

            // If the Frame has expired by the time we process this event, return.
            if (depthFrame == null)
            {
                return;
            }

            // We use a try/finally to ensure that we clean up before we exit the function.  
            // This includes calling Dispose on any Frame objects that we may have and unlocking the bitmap back buffer.
            try
            {
                ProcessingDepth(depthFrame);

                if (this.save && this.saveDepth)
                {
                    depthTasks.Add(depthStream.WriteFrameAsync(true, (byte[])depthPixels.Clone(), 0, depthPixels.Length));
                }
            }
            finally
            {
                if (depthFrame != null)
                {
                    depthFrame.Dispose();
                }
            }
        }

        /// <summary>
        /// Receives and processes a new InfraredFrame captured by the Kinect.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_InfraredFrameArrived(object senser, InfraredFrameArrivedEventArgs e)
        {
            InfraredFrame infaredFrame = e.FrameReference.AcquireFrame();


            // If the Frame has expired by the time we process this event, return.
            if (infaredFrame == null)
            {
                return;
            }

            // We use a try/finally to ensure that we clean up before we exit the function.  
            // This includes calling Dispose on any Frame objects that we may have and unlocking the bitmap back buffer.
            try
            {
                ProcessingInfared(infaredFrame);

                if(this.save && this.saveInfrared){
                    infraredTasks.Add(infraredStream.WriteFrameAsync(true, (byte[])infraredPixels.Clone(), 0, infraredPixels.Length));
                }
 
            }
            finally
            {
                if(infaredFrame != null)
                {
                    infaredFrame.Dispose();
                }
            }
        }

        /// <summary>
        /// Receives and processes a new BodyIndexFrame captured by the Kinect.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_BodyIndexFrameArriver(object sender, BodyIndexFrameArrivedEventArgs e)
        {
            BodyIndexFrame bodyIndexFrame = e.FrameReference.AcquireFrame();


            // If the Frame has expired by the time we process this event, return.
            if (bodyIndexFrame == null)
            {
                return;
            }

            // We use a try/finally to ensure that we clean up before we exit the function.  
            // This includes calling Dispose on any Frame objects that we may have and unlocking the bitmap back buffer.
            try
            {
                ProcessingShadow(bodyIndexFrame);

                if (this.save && this.saveShadow)
                {
                    shadowTasks.Add(shadowStream.WriteFrameAsync(true, (byte[])shadowPixels.Clone(), 0, shadowPixels.Length));
                }
            }
            finally
            {
                if (bodyIndexFrame != null)
                {
                    bodyIndexFrame.Dispose();
                }
            }
        }

        /// <summary>
        /// Receives and processes a new BodyFrame captured by the Kinect.
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Reader_BodyFrameArrived(object sender, BodyFrameArrivedEventArgs e)
        {
            BodyFrame bodyFrame = e.FrameReference.AcquireFrame();

            // If the Frame has expired by the time we process this event, return.
            if (bodyFrame == null)
            {
                return;
            }

            // We use a try/finally to ensure that we clean up before we exit the function.  
            // This includes calling Dispose on any Frame objects that we may have and unlocking the bitmap back buffer.
            try
            {
                double timestamp = bodyFrame.RelativeTime.TotalSeconds;
                ProcessingBodies(bodyFrame, timestamp);

                if (this.save && this.saveSkeleton)
                {
                    

                }

            }
            finally
            {
                if (bodyFrame != null)
                {
                    bodyFrame.Dispose();
                }
            }
        }

        /// <summary>
        /// Handles the event which the sensor becomes unavailable (E.g. paused, closed, unplugged).
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void Sensor_IsAvailableChanged(object sender, IsAvailableChangedEventArgs e)
        {
            this.StatusText = this.kinectSensor.IsAvailable ? Properties.Resources.RunningStatusText
                                                            : Properties.Resources.SensorNotAvailableStatusText;
        }
    }
}
