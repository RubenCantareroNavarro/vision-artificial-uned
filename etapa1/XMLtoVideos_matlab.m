XMLpath = '/Volumes/SHARED/Datasets/RecogidaDatosKinectV2/RecogidaDatos/videos/actores/actor17/depth/xml/';% Name it.
XMLfiles = dir(strcat(XMLpath , '*.xml'));

fId = figure;
hold;
xlabel('x');
ylabel('y');
zlabel('z');

ru=0;

for aux = 1:25
    pl(aux) = plot3(0,0,0,'.');
end

writerObj = VideoWriter('/Users/rubencn90/Desktop/actor17_Skeleton.avi'); % Name it.
writerObj.FrameRate = 25; % How many frames per second.
open(writerObj); 

for XMLfile = XMLfiles'
    try
        xDoc = xmlread(strcat(XMLpath , XMLfile.name));
        positions = xDoc.getElementsByTagName('position');

        figure(fId); % Makes sure you use your desired frame.

        if positions.getLength > 0
            for k = 0:24
               position = positions.item(k);

               positionx = position.getElementsByTagName('positionx');
               thisElementx = positionx.item(0);
               x = thisElementx.getFirstChild.getData;

               positionx = position.getElementsByTagName('positiony');
               thisElementy = positionx.item(0);
               y = thisElementy.getFirstChild.getData;

               positionx = position.getElementsByTagName('positionz');
               thisElementz = positionx.item(0);
               z = thisElementz.getFirstChild.getData;

               x = str2double(strrep(char(x),',','.'));
               y = str2double(strrep(char(y),',','.'));
               z = str2double(strrep(char(z),',','.'));

               axis([-2 2 -2 2])

               set(pl(k+1), 'XData', x, 'YData', y, 'ZData', z);
            end
        else 
            for k = 0:24
                set(pl(k+1), 'XData', 0, 'YData', 0, 'ZData', 0);
            end
        end

        frame = getframe(gcf); % 'gcf' can handle if you zoom in to take a movie.
        writeVideo(writerObj, frame);

        ru
        ru = ru +1;
    catch
        fid = fopen( '/Users/rubencn90/Desktop/log_file_empty.txt', 'at' );
        fprintf(fid, '%s\n', strcat(XMLpath , XMLfile.name));
        fclose(fid);
        warning('The document %s is empty.', strcat(XMLpath , XMLfile.name));
    end
end

hold off
close(writerObj); % Saves the movie.
