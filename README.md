# Práctica M2 #

Repositorio que contiene todo el material generado/utilizado para la elaboración de la práctica M2 de la asignatura. La memoria puede ser descargada en el siguiente [enlace](https://www.dropbox.com/s/fofp9ht1jf3k1dn/Ruben_Cantarero_Navarro_M2.pdf?dl=0). Este repositorio se estructura en 4 directorios, que se corresponden a su vez con las cuatro etapas a través de las cuales ha sido afrontada la práctica:

* **etapa1:** Contiene una carpeta con el proyecto de [Visual Studio](https://www.visualstudio.com/es/?rr=https%3A%2F%2Fwww.google.es%2F) del módulo de captura de imágenes y un script de Matlab para generar vídeos de esqueletos a partir de los ficheros XML capturados.
* **etapa2:** Contiene en enlace desde el cual puede ser descargado el dataset generado. Es importante destacar que se trata de un archivo comrprimido (.zip) de gran tamaño (25,6 GB).
* **etapa3:** Contiene todo el código y material necesario para la ejecución de las evaluaciones realizadas en la tercera etapa.
* **etapa4:** Contiene todo el código y material necesario para la ejecución de las evaluaciones realizadas en la cuarta etapa.


## Manual de usuario ##

### Etapa 1 ###

Para ejecutar el módulo de captura de imágenes para la segunda generación de Kinect, siga los siguientes pasos:

1. Abrir el entorno Visual Studio.
2. Abrir el proyecto contenido en el directorio /etapa1
3. Ejecute el proyecto.
4. Seleccionar los flujos de datos a capturar e indicar la ruta donde desea guardarlos y el nombre base de las capturas.
5. Presionar el botón "*Start Save*".

### Etapa 2 ###

Para poder trabajar con el dataset generado, siga los siguientes pasos:

1. Abrir el enlace indicado en el fichero del directorio /etapa2.
2. Esperar hasta que este completamente descargado el fichero "*KinbehrDataset_v2.zip*".
3. Abrir y descomprimir el fichero descargado con un programa de descompresión (como por ejemplo [7zip](https://www.google.es/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwiJuLaBqJ3UAhWDuBQKHQyyD04QFggoMAA&url=http%3A%2F%2Fwww.7-zip.org%2F&usg=AFQjCNElgl3u-wgL-ZOjo_CH4znsHpsyyw&sig2=n7xJZaJ32V3wHiwhM_s1Pw)) en la ruta deseada.

### Etapa 3 ###

Para poder ejecutar las evaluaciones realizadas durante la tercera etapa, siga los siguientes pasos:

1. Abrir el entorno [Matlab](https://www.google.es/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwip0d-RrJ3UAhWJUhQKHae7BpQQFggwMAA&url=https%3A%2F%2Fes.mathworks.com%2Fproducts%2Fmatlab.html&usg=AFQjCNH_fso-mtwU4rrKOGMqs3L6iHRAcA&sig2=PCqrPK2tiyCx1fQ_WotSTg).
2. Abrir el proyecto scpit "*etapa3.m*" que se encuentra en el directorio /etapa3.
3. Ejecute el script. Nota: Asegúrese que las rutas del archivo que contiene el groundTruth es accesible por Matlab.
4. Sigas las instrucciones mostradas en pantalla para pasar a la siguiente imagen o terminar con la evaluación.

En el directorio correspondiente a esta etapa (/etapa3) se encuentran los siguientes ficheros:

* **chair.png:** imagen de referencia para detectar la silla.
* **etapa3.m:** script que contiene el código necesario para ejecutar las evaluaciones de la presente etapa.
* **groundTruth_Chair.mat:** contiene el groundTruth para la silla.
* **groundTruth_punchingBall.mat** contiene el groundTruth para el punching-ball. Para más información, consulte la memoria. 
* **punchingball.png:** imagen de referencia para detectar el punching-ball. Para más información, consulte la memoria.
* **trainingImageLabeler_sesion_chair.mat:** contiene la sesión del groundTruth generada por la herramienta *[Training Image Labeler](https://es.mathworks.com/help/vision/ug/label-images-for-classification-model-training.html)* de Matlab.
* **imagenes_seleccionadas:** directorio que contiene las imagenes en formato *png* seleccionadas para evaluar el experimento de la etapa 3 (extraídas del datase de la etapa 2). Para más información, consulte la memoria.

### Etapa 4 ###

Para poder ejecutar las evaluaciones realizadas durante la cuarta etapa, siga los siguientes pasos:

1. Si se encuentra en una distribución GNU/Linux, siga los pasos de instalación adjuntos en el Anexo I de la memoria.
2. Ejecute los siguientes comandos:

```
$ make 
$ cd etapa4/src/
$ ./correspondence_grouping ../points_cloud/punching_entero.pcd ../points_cloud/escena.pcd 

```


## Datos del Alumno ##
* **Asigntaura:** Visión Artificial.
* **Máster:** MÁSTER UNIVERSITARIO EN I.A. AVANZADA: FUNDAMENTOS,MÉTODOS Y APLICACIONES
* **Alumno:** Rubén Cantarero Navarro.
* **DNI:** 50480116S.
* **Curso:** 2016-2017.