%% Alumno: Rubén Cantarero Navarro
%% DNI: 50480116S
%% Asignatura: Visión Artificial.
%% Importante: BAsado en el ejemplo de "Object Detection in a Cluttered Scene Using Point Feature Matching" de The MathWorks, Inc. 

%% Ground Truth (ROIs) obtained to test solutions. Obtained with trainingImageLabeler app.
groundTruth = importdata('groundTruth.mat');

%% Read the reference image containing the object of interest.
boxImage = rgb2gray(imread('punchingball.png'));
figure;
imshow(boxImage);
title('Image of the object of interest');

%% Detect feature points in reference image
boxPoints = detectSURFFeatures(boxImage);

%% Visualize the strongest feature points found in the reference image.
figure; 
imshow(boxImage);
title('100 Strongest Feature Points from object of interest');
hold on;
plot(selectStrongest(boxPoints, 100));

%% Extract feature descriptors at the interest points in both images.
[boxFeatures, boxPoints] = extractFeatures(boxImage, boxPoints);

%% Test the scnene's images to detect the object of interest.
total = size(groundTruth,1);
notSufficient = 0;
sucess = 0;
pause(0.5);
for i=1: size(groundTruth,1)
    %% Read the scene image to test.
    path = groundTruth.imageFilename(i);
    display(path{1});
    sceneImage = rgb2gray(imread(path{1}));
    
    %% Detect feature points in both images.
    scenePoints = detectSURFFeatures(sceneImage);

    %% Extract feature descriptors at the interest points in both images.
    [sceneFeatures, scenePoints] = extractFeatures(sceneImage, scenePoints);

    %% Match the features using their descriptors. 
    boxPairs = matchFeatures(boxFeatures, sceneFeatures);
    
    %% Display putatively matched features. 
    matchedBoxPoints = boxPoints(boxPairs(:, 1), :);
    matchedScenePoints = scenePoints(boxPairs(:, 2), :);
    
    %% Locate the Object in the Scene Using Putative Matches
    % |estimateGeometricTransform| calculates the transformation relating the
    % matched points, while eliminating outliers. This transformation allows us
    % to localize the object in the scene.
    
    if matchedBoxPoints.Count >=3 && matchedScenePoints.Count
        [tform, inlierBoxPoints, inlierScenePoints] = ...
           estimateGeometricTransform(matchedBoxPoints, matchedScenePoints, 'affine');
    else
        disp('Not sufficient points yo estimate.');
        notSufficient = notSufficient + 1;
        continue;
    end
    
    % Get the bounding polygon of the reference image.
    boxPolygon = [1, 1;...                           % top-left
            size(boxImage, 2), 1;...                 % top-right
            size(boxImage, 2), size(boxImage, 1);... % bottom-right
            1, size(boxImage, 1);...                 % bottom-left
            1, 1];                   % top-left again to close the polygon
    
    %% Transform the polygon into the coordinate system of the target image.
    % The transformed polygon indicates the location of the object in the
    % scene.
    newBoxPolygon = transformPointsForward(tform, boxPolygon);    
  
    %% Display the detected object.
    handles.H = figure;
    imshow(sceneImage);
    hold on;
    line(newBoxPolygon(:, 1), newBoxPolygon(:, 2), 'Color', 'y');
    rectangle('Position',groundTruth.punchingball(i,:))
    title(path{1});
                
    % Construct a questdlg with three options
    choice = questdlg('¿Estimación correcta?', ...
        'Evalución', ...
        'Si', 'No', 'Finalizar', 'Finalizar');
    % Handle response
    switch choice
        case 'Si'
            disp(['Sucess: ' sucess])
            sucess = sucess + 1;
        case 'No'
            disp('Not Sucess.')
        case 'Finalizar' 
            disp('Finalizar.')
            return;
    end
    close(handles.H)
    pause(0.5)

end

%% Calculate the sucess of tests
sucessRate = sucess/total;
